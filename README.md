#ngx_http_abtest_module

usage:

<pre>

server {
	listen 	8080;

	location = /percent_test {
		abtest percent key=arg_id {
			30		/test_a;
			70		/test_b;
		}
	}

	location = /match_test {
		abtest match key=arg_id {
			^a(.*)	/test_a 	default;
			^abc	/test_b;
		}
	}

	location /test_a {
		echo			"test_a\n";
	}

	location /test_b {
		echo	  		"test_b\n";
	}
}

</pre>


WARNING: this module will be called after ngx_http_rewrite_module, use it carefully.

the abtest directive only allowed in location block, named location is supported, custom args is not support now,
the key is all variables that nginx support, but you do not need to add "$", the "default" param is used to rewrite the request which do not have the key.
