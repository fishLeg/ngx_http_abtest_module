#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

uint32_t
ngx_murmur_hash2(char *data, size_t len)
{
    uint32_t  h, k;

    h = 0 ^ len;

    while (len >= 4) {
        k  = data[0];
        k |= data[1] << 8;
        k |= data[2] << 16;
        k |= data[3] << 24;

        k *= 0x5bd1e995;
        k ^= k >> 24;
        k *= 0x5bd1e995;

        h *= 0x5bd1e995;
        h ^= k;

        data += 4;
        len -= 4;
    }

    switch (len) {
    case 3:
        h ^= data[2] << 16;
    case 2:
        h ^= data[1] << 8;
    case 1:
        h ^= data[0];
        h *= 0x5bd1e995;
    }

    h ^= h >> 13;
    h *= 0x5bd1e995;
    h ^= h >> 15;

    return h;
}

int main(int argc, const char *argv[]) {
	uint32_t			hash, slot;
	float				loc;

	if(argc != 2) {
		printf("usage: ./percent_test xxxxx\n");
		return 1;
	}

	hash = ngx_murmur_hash2((char *)argv[1], strlen(argv[1]));
	slot = hash & ((8 * 1024) - 1);

	loc = (float)((float)slot / (float)(8 * 1024));
	printf("hash:    %u\n", hash);
    printf("slot:    %u\n", slot);
	printf("loc :    %f\n", loc * 100);

	return 0;
}
